use std::io;
use std::cmp::Ordering;

// we use a rand crate apparently bc it's not part of the std yet
use rand::Rng;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {
        println!("Please input your guess.");

        let mut guess = String::new();

        // I like how you explicitly pass by reference or mutable reference when calling a fn
        io::stdin().read_line(&mut guess).expect("Failed to read line");
        // I guess .expect is a method of Result, the return type here is Result<usize> and is the
        // number of bytes read. Expect would return the value if it exists, otherwise panics with
        // the message passed in if its enum type is Err

        // shadow old var
        let guess: u32 = match guess.trim().parse() {
            // best way to handle std::result type imo
            Ok(num) => num,
            Err(_) => {
                println!("Please type a number!");
                continue
            }
        };

        // didn't know about this way of doing format strings
        println!("You guessed: {guess}");

        // this is cool
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
