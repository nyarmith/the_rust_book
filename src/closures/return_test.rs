fn return_closure<'a>() -> &'a dyn Fn(i32) -> i32 {
    &|x| x+1
}

fn main() {
    let my_fn = return_closure();
    println!("{}", my_fn(2));
}
