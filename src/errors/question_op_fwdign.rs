use std::fs::File;
use std::io;
use std::io::Read;

fn read_username_from_file() -> Result<String, io::Error> {
    let mut username = String::new();

    File::open("hello.txt")?.read_to_string(&mut username)?;

    Ok(username)
}

fn main() {
    match read_username_from_file() {
        Ok(e) => println!("{}",e),
        Err(e) => println!("oops")
    }
}
