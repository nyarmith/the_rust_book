use std::env;
use std::process;

use minigrep::Config;

fn main() {
    let args: Vec<String> = env::args().collect();  // note that this panics if any arg contains
                                                    // invalid unicode, get around this by using
                                                    // std::env::args_os, which returns an iterator
                                                    // that produces OsString values instead of
                                                    // String vals. Interesting.
    let config = Config::build(&args).unwrap_or_else(|err| {
        println!("Problem parsing arguments: {err}");
        process::exit(1);
    });

    if let Err(e) = minigrep::run(config) {
        println!("Application error: {e}");
        process::exit(1);
    }
}

