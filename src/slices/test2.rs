fn main() {
  let s = String::from("hello");
  let s2: &String = &s;
  let s3: &str = &s[..];
  println!("memory of s2 {}", std::mem::size_of_val(s2));
  println!("memory of s3 {}", std::mem::size_of_val(s3));
  println!(
    "size of types -- &String={} &str={}",
    std::mem::size_of::<&String>(),
    std::mem::size_of::<&str>(),
  );
}
